//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
    /*Icon*/	/*Command*/		                        /*Update Interval*/	/*Update Signal*/
	{" : ", "free -h | awk '/^Mem/ { print $3\"/ \"$2 }' | sed s/i//g",	10,		0},
    /*{" : ", "free -h | awk '/^Swap/ { print $3 \" / \"$2 }' | sed s/i//g ", 10, 0},
    {" : ", "top -b -n1 | grep 'Cpu(s)' | awk '{print $2 + $4}' ", 1, 0 },*/
    {"  ", "awk '{print $0\"%\"}' /sys/class/power_supply/BAT0/capacity", 1, 0},
    {"", "date '+%b %d (%a) %I:%M%p'",            5,    0},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = "   ";
static unsigned int delimLen = 5;
